package com.guidewire.coding;

import com.guidewire.coding.exception.LineValidationException;
import com.guidewire.coding.service.CSVToObjectParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class CodingApplication {
    private final static Logger LOGGER =
            Logger.getLogger(CodingApplication.class.getName());

    public static void main(String[] args) throws LineValidationException {
        SpringApplication.run(CodingApplication.class, args);
        try{
            CSVToObjectParser parser = new CSVToObjectParser();
            parser.parseData(args[0]);
        }catch (Exception e){
            LOGGER.log(Level.SEVERE, "File error: "+e.getMessage());
        }
    }
}
