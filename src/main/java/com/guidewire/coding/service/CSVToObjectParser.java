package com.guidewire.coding.service;

import com.guidewire.coding.constant.RegexConstants;
import com.guidewire.coding.exception.LineValidationException;
import com.guidewire.coding.model.Entry;
import com.guidewire.coding.model.JsonResponse;
import com.guidewire.coding.util.CSVFileReader;
import com.guidewire.coding.util.JsonParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;

public class CSVToObjectParser {

    public JsonResponse parseData(String fileName) throws LineValidationException {
        JsonResponse jsonResponse = new JsonResponse();
        List<List<String>> parsedString = CSVFileReader.readCSVFile(fileName);
        List<Integer> errors = new ArrayList<>();
        List<Entry> entries = new ArrayList<>();
        int lineCount = 0;
        for (List<String> lineObject : parsedString) {
            parseLineObject(lineObject, ++lineCount, errors, entries);
        }
        Collections.sort(entries, Comparator.comparing(Entry::getLastName)
                .thenComparing(Entry::getFirstName));

        jsonResponse.setEntries(entries);
        jsonResponse.setErrors(errors);
        System.out.println(JsonParser.parseObjectToJson(jsonResponse));
        return jsonResponse;
    }

    public void parseLineObject(List<String> lineObject, Integer lineCount, List<Integer> errors, List<Entry> entries) {
        Entry entry = new Entry();
        try {
            if (lineObject.size() < 2) {
                errors.add(lineCount);
            } else if (lineObject.size() == 5) {
                entry.setFirstName(lineObject.get(1).trim());
                entry.setLastName(lineObject.get(0).trim());
                validationSeries(lineObject.subList(2, lineObject.size()), entry);
            } else if (lineObject.size() == 4) {
                parseName(lineObject.get(0), entry);
                validationSeries(lineObject.subList(1, lineObject.size()), entry);
            }
            if (entry.getFirstName() != null) {
                entries.add(entry);
            }
        } catch (LineValidationException e) {
            errors.add(lineCount);
        }

    }

    private void parseName(String name, Entry entry) {
        String[] names = name.split(" ");
        entry.setFirstName(names[0].trim());
        entry.setLastName(names[1].trim());
    }

    private void validationSeries(List<String> lineObject, Entry entry) throws LineValidationException {
        for (String input : lineObject) {
            input = input.trim();
            if (validateZipCode(input)) {
                entry.setZipcode(input);
                continue;
            } else if (validatePhoneNumber(input)) {
                entry.setPhoneNumber(input);
                continue;
            } else if (validateColor(input)) {
                entry.setColor(input);
                continue;
            } else {
                throw new LineValidationException("Value in the line is not matching for a field");
            }
        }
    }

    private boolean validateZipCode(String possibleZip) {
        Matcher matcher = RegexConstants.zipCodePattern.matcher(possibleZip);
        return matcher.matches();
    }

    private boolean validatePhoneNumber(String possiblePhoneNumber) {
        Matcher matcher = RegexConstants.phoneNumberPattern.matcher(possiblePhoneNumber);
        return matcher.matches();
    }

    private boolean validateColor(String possibleColor) {
        Matcher matcher = RegexConstants.colorPattern.matcher(possibleColor);
        return matcher.matches();
    }

}
