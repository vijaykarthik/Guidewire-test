package com.guidewire.coding.constant;

import java.util.regex.Pattern;

public class RegexConstants {
    static String zipCodeRegex = "^[0-9]{5}(?:-[0-9]{4})?$";
    static String phoneNumberRegex = "^((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
    static String colorRegex = "[a-z A-Z]+";

    public static Pattern phoneNumberPattern = Pattern.compile(phoneNumberRegex);
    public static Pattern zipCodePattern = Pattern.compile(zipCodeRegex);
    public static Pattern colorPattern = Pattern.compile(colorRegex);
}
