package com.guidewire.coding.exception;

public class LineValidationException extends Exception {

    public LineValidationException(Exception e) {
        super(e);
    }

    public LineValidationException(String message) {
        super(message);
    }
}
