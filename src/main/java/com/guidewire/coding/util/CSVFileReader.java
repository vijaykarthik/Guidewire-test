package com.guidewire.coding.util;

import com.guidewire.coding.exception.LineValidationException;
import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVFileReader {
    public static final String COMMA_DELIMITER = ",";

    public static List<List<String>> readCSVFile(String fileName) throws LineValidationException {
        List<List<String>> records = new ArrayList<>();
        validateFile(fileName);
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.trim().isBlank()) {
                    String[] values = line.split(COMMA_DELIMITER);
                    records.add(Arrays.asList(values));
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException in readCSVFile() of CSVFilerReader: " + e.getMessage());
            throw new LineValidationException(e);
        } catch (IOException e) {
            System.out.println("IOException in readCSVFile() of CSVFilerReader: " + e.getMessage());
            throw new LineValidationException(e);
        }
        return records;
    }

    private static void validateFile(String fileName) throws LineValidationException {
        File file = new File(fileName);
        if(!file.isFile()){
            throw new LineValidationException("Given file is not valid");
        }

        String extension = FilenameUtils.getExtension(fileName);
        if(!(extension.equalsIgnoreCase("txt") || extension.equalsIgnoreCase("csv"))){
            throw new LineValidationException("File is not with valid extension");
        }
    }
}
