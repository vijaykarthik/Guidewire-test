package com.guidewire.coding.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guidewire.coding.model.JsonResponse;

public class JsonParser {

    public static String parseObjectToJson(JsonResponse jsonResponse) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonResponse);
        } catch (JsonProcessingException e) {
            System.out.println();
        }
        return null;
    }
}


