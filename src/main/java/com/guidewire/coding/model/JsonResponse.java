package com.guidewire.coding.model;

import java.util.List;

public class JsonResponse {
    List<Entry> entries;
    List<Integer> errors;

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public List<Integer> getErrors() {
        return errors;
    }

    public void setErrors(List<Integer> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "JsonResponse{" +
                "entries=" + entries +
                ", errors=" + errors +
                '}';
    }
}
