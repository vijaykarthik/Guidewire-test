package com.guidewire.coding.controller;

import com.guidewire.coding.exception.LineValidationException;
import com.guidewire.coding.model.JsonResponse;
import com.guidewire.coding.service.CSVToObjectParser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class CSVFileController {
    private static final String FILE_NAME = "/Users/Vijay/Downloads/coding/src/main/resources/data.txt";

    @GetMapping
    public JsonResponse parseFile() throws LineValidationException {
        CSVToObjectParser csvToObjectParser = new CSVToObjectParser();
        csvToObjectParser.parseData(FILE_NAME);
        return null;
    }
}
