package com.guidewire.coding.service;

import com.guidewire.coding.exception.LineValidationException;
import com.guidewire.coding.model.Entry;
import com.guidewire.coding.model.JsonResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class CSVToObjectParserTest {

    CSVToObjectParser csvToObjectParser = new CSVToObjectParser();

    /* Testing with csv file extension */
    @Test
    void parseDataWithCSVFileExtension() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/CSV File.csv");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(1, jsonResponse.getEntries().size());
    }

    /* Testing with txt file extension in UPPERCASE */
    @Test
    void parseDataWithTXTFileExtensionInUpperCase() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/DataWithTxtInCaps.TXT");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(1, jsonResponse.getEntries().size());
    }

    /* Testing with csv file extension in UPPERCASE */
    @Test
    void parseDataWithCSVFileExtensionInUpperCase() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/DataWithCSVInCaps.CSV");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(1, jsonResponse.getEntries().size());
    }

    /* Testing with xml file extension */
    @Test
    void parseDataWithXMLFileExtension() throws LineValidationException {
        Assertions.assertThrows(LineValidationException.class, ()-> csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/XML File.xml"));
    }

    /* Testing file with one error */
    @Test
    void parseDataWith1Error() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/1ErrorData.txt");
        Assertions.assertEquals(1, jsonResponse.getErrors().size());
        Assertions.assertEquals(0, jsonResponse.getEntries().size());
    }

    /* Testing file with one correct data */
    @Test
    void parseDataWithNoError() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/1Entry.txt");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(1, jsonResponse.getEntries().size());
    }

    /* Testing with an empty file */
    @Test
    void parseDataWithEmptyFile() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/EmptyFile.txt");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(0, jsonResponse.getEntries().size());
    }

    /* Testing with multiple empty lines */
    @Test
    void parseDataWithEmptyFileWith10EmptyLinesFile() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/EmptyFileWith10EmptyLines.txt");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(0, jsonResponse.getEntries().size());
    }

    /* Testing with spaces in lines instead of just empty lines */
    @Test
    void parseDataWithFileWithSpacesInLines() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/FileWithSpacesInLines.txt");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(0, jsonResponse.getEntries().size());
    }

    /* Testing file with combination of valid and empty or line with spaces */
    @Test
    void parseDataWithEmptyAndValidLines() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/FileWithValidAndEmptyContent.txt");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(2, jsonResponse.getEntries().size());
    }

    /* Testing file to check the order of the result. Key lastName and firstName is the order.
     *  If Lastname is same, then entry is ordered by first name */
    @Test
    void parseDataToCheckOrderOfKey() throws LineValidationException {
        JsonResponse jsonResponse = csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/FileToCheckOrderOfKey.txt");
        Assertions.assertEquals(0, jsonResponse.getErrors().size());
        Assertions.assertEquals(4, jsonResponse.getEntries().size());
        Entry first = jsonResponse.getEntries().get(0);
        Assertions.assertEquals("B", first.getFirstName());
        Assertions.assertEquals("A", first.getLastName());
        Entry second = jsonResponse.getEntries().get(1);
        Assertions.assertEquals("A", second.getFirstName());
        Assertions.assertEquals("C", second.getLastName());
        Entry third = jsonResponse.getEntries().get(2);
        Assertions.assertEquals("B", third.getFirstName());
        Assertions.assertEquals("C", third.getLastName());
        Entry fourth = jsonResponse.getEntries().get(3);
        Assertions.assertEquals("A", fourth.getFirstName());
        Assertions.assertEquals("Z", fourth.getLastName());
    }

    /* Testing to see exception occurs if given file is not found */
    @Test
    void parseDataWithInvalidFile() throws LineValidationException {
        Assertions.assertThrows(LineValidationException.class, () -> csvToObjectParser.parseData("/Users/Vijay/Downloads/coding/src/test/resources/InvalidFile.txt"));
    }

    /* Testing parseLine with empty input in list */
    @Test
    void parseLineObjectWithEmptyInput() {
        List<Entry> entryList = new ArrayList<>();
        List<Integer> errors = new ArrayList<>();
        csvToObjectParser.parseLineObject(Arrays.asList(" "), 0, errors, entryList);
        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(0, entryList.size());
    }

    /* Testing parseLine with correct list of string where lastname and firstname are
     * separated by comma */
    @Test
    void parseLineObjectWith1EntryOfSeparateFirstAndLastName() {
        List<Entry> entryList = new ArrayList<>();
        List<Integer> errors = new ArrayList<>();
        List<String> line = Arrays.asList(new String[]{"lname", "fname", "54321", "1234567890", "blue"});
        csvToObjectParser.parseLineObject(line, 0, errors, entryList);
        Assertions.assertEquals(0, errors.size());
        Assertions.assertEquals(1, entryList.size());
        Entry entry = entryList.get(0);
        Assertions.assertEquals("fname", entry.getFirstName());
        Assertions.assertEquals("lname", entry.getLastName());
        Assertions.assertEquals("54321", entry.getZipcode());
        Assertions.assertEquals("1234567890", entry.getPhoneNumber());
        Assertions.assertEquals("blue", entry.getColor());
    }

    /* Testing parseLine with correct list of string where firstname and lastname are
     * separated by space */
    @Test
    void parseLineObjectWithEntryOfMergedFirstAndLastName() {
        List<Entry> entryList = new ArrayList<>();
        List<Integer> errors = new ArrayList<>();
        List<String> line = Arrays.asList(new String[]{"fname lname", "54321", "1234567890", "blue"});
        csvToObjectParser.parseLineObject(line, 0, errors, entryList);
        Assertions.assertEquals(0, errors.size());
        Assertions.assertEquals(1, entryList.size());
        Entry entry = entryList.get(0);
        Assertions.assertEquals("fname", entry.getFirstName());
        Assertions.assertEquals("lname", entry.getLastName());
        Assertions.assertEquals("54321", entry.getZipcode());
        Assertions.assertEquals("1234567890", entry.getPhoneNumber());
        Assertions.assertEquals("blue", entry.getColor());
    }

    /* Testing parseLine with correct list of string to check with values in
     * different order */
    @Test
    void parseLineObjectWithEntryOfDifferentOrder() {
        List<Entry> entryList = new ArrayList<>();
        List<Integer> errors = new ArrayList<>();
        List<String> line = Arrays.asList(new String[]{"lname", "fname", "1234567890", "blue", "54321"});
        csvToObjectParser.parseLineObject(line, 0, errors, entryList);
        Assertions.assertEquals(0, errors.size());
        Assertions.assertEquals(1, entryList.size());
        Entry entry = entryList.get(0);
        Assertions.assertEquals("fname", entry.getFirstName());
        Assertions.assertEquals("lname", entry.getLastName());
        Assertions.assertEquals("54321", entry.getZipcode());
        Assertions.assertEquals("1234567890", entry.getPhoneNumber());
        Assertions.assertEquals("blue", entry.getColor());
    }

    /* Testing parseLine with incorrect list of string to check error occurs */
    @Test
    void parseLineObjectWithSingleValueInLine() {
        List<Entry> entryList = new ArrayList<>();
        List<Integer> errors = new ArrayList<>();
        List<String> line = Arrays.asList(new String[]{"lname"});
        csvToObjectParser.parseLineObject(line, 0, errors, entryList);
        Assertions.assertEquals(1, errors.size());
        Assertions.assertEquals(0, entryList.size());
    }
}